import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import '../database/sql_helper.dart';
import '../widget/Affiche.dart';

class Echec extends StatefulWidget {
  const Echec({Key key}) : super(key: key);

  @override
  State<Echec> createState() => _EchecState();
}

class _EchecState extends State<Echec> {

  // notification

  void envoinofification(String title , String body) async {
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

    //
    const AndroidInitializationSettings initializationSettingsAndroid =
    AndroidInitializationSettings('@mipmap/smile');
    const IOSInitializationSettings initializationSettingsIOS =
    IOSInitializationSettings(
      requestSoundPermission: true,
      requestBadgePermission: true,
      requestAlertPermission: true,
    );
    final MacOSInitializationSettings initializationSettingsMacOS =
    MacOSInitializationSettings();
    final InitializationSettings initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsIOS,
        macOS: initializationSettingsMacOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
    );

    // android

    AndroidNotificationChannel channel = AndroidNotificationChannel(
        'ok',  'okkk', description: "verifie", importance: Importance.max
    );

    flutterLocalNotificationsPlugin.show(0,title,body,NotificationDetails(
        android: AndroidNotificationDetails(channel.id , channel.name, )
    )
    );


  }

  //


  // All journals
  List<Map<String, dynamic>> _journals = [];

  bool _isLoading = true;
  bool _isActive = true;
  // This function is used to fetch all data from the database
  void _refreshJournals() async {
    final data = await SQLHelper.getItems();
    setState(() {
      _journals = data;
      _isLoading = false;
      _isActive = false ;
    });
  }

  @override
  void initState() {
    super.initState();
    envoinofification;
    _refreshJournals(); // Loading the diary when the app starts
  }

  final TextEditingController _nomController = TextEditingController();
  final TextEditingController _urlController = TextEditingController();



  // This function will be triggered when the floating button is pressed
  // It will also be triggered when you want to update an item

// Insert a new journal to the database


  void _refreshForm(int url) async {
    await SQLHelper.refreshFormItem(url);
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text('Active !'),
    ));

    _refreshJournals();


  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Tester Api en direct '),
      ),
      body: Affiche(),
      //bottomNavigationBar: NavBar(),
    );
  }
}