import 'package:flutter/foundation.dart';
import 'package:sqflite/sqflite.dart' as sql;
import 'package:http/http.dart' as http;
import 'package:path/path.dart';
import 'package:path/path.dart' as p;
import 'package:collection/collection.dart';
import 'package:sqflite/sqflite.dart';


class SQLHelper {
  static Future<void> createTables(sql.Database database) async {
    await database.execute("""CREATE TABLE items(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        nom TEXT,
        url TEXT,
        createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
      )
      """);
  }
// id: the id of a item
// title, description: name and description of your activity
// created_at: the time that the item was created. It will be automatically handled by SQLite

  static Future<sql.Database> db() async {
    return sql.openDatabase(
      'angesmile.db',
      version: 1,
      onCreate: (sql.Database database, int version) async {
        await createTables(database);
      },
    );
  }

  // creer  nouveau  item
  static Future<int> createItem(String nom, String url) async {
    final db = await SQLHelper.db();

    final data = {'nom': nom, 'url': url};
    final id = await db.insert('items', data,
        conflictAlgorithm: sql.ConflictAlgorithm.replace);
    return id;
  }

  // lire tous les  items
  static Future<List<Map<String, dynamic>>> getItems() async {
    final db = await SQLHelper.db();
    return db.query('items', orderBy: "id");
  }

  // Read a single item by id
  // The app doesn't use this method but I put here in case you want to see it
  static Future<List<Map<String, dynamic>>> getItem(int id) async {
    final db = await SQLHelper.db();
    return db.query('items', where: "id = ?", whereArgs: [id], limit: 1);
  }

  // modifier  item  id
  static Future<int> updateItem(
      int id, String nom, String url) async {
    final db = await SQLHelper.db();

    final data = {
      'nom': nom,
      'url': url,
      'createdAt': DateTime.now().toString()
    };

    final result =
    await db.update('items', data, where: "id = ?", whereArgs: [id]);
    return result;
  }

  // Delete
  static Future<void> deleteItem(int id) async {
    final db = await SQLHelper.db();
    try {
      await db.delete("items", where: "id = ?", whereArgs: [id]);
    } catch (err) {
      debugPrint("Something went wrong when deleting an item: $err");
    }
  }

  //count
  static Future<int> getCoun() async{
    var db = await SQLHelper.db();
    List<Map<String, dynamic>> x = await db.rawQuery("SELECT COUNT (*) from items");
    int count = Sqflite.firstIntValue(x);
    print(
      '$count'
    );
    return count;
  }

  static Future<int> getCount() async{
    var db = await SQLHelper.db();
    final count = Sqflite.firstIntValue(await db.rawQuery("SELECT COUNT(*) from items"));
      print('$count');
    return count;
  }



  //api url verification (non actif)



  static Future<void> refreshFormItem(int url) async{
    final response = await http.post(Uri.parse("https://test132smile.000webhostapp.com/ajoutApp.php"

    ),
    );
    try{
      if (response.statusCode == 200){
        print("ok");
      }else{
        print("erreur");
      }
    } catch (err) {
      debugPrint("Erreur : $err");
    }

  }

  //

// fontions de recherche
  static Future<List> search(String app) async{
    var db = await SQLHelper.db();
    List<Map<String, dynamic>> search = await db.query('items', where: 'nom LIKE ?', whereArgs: ['%$app%']);
    return search;
  }
// Grud projet



// fin grud projet

}