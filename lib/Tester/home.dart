

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../Processus/12h.dart';
import '../database/sql_helper.dart';

class Appadd extends StatefulWidget {
  const Appadd({Key key}) : super(key: key);

  @override
  State<Appadd> createState() => _AppaddState();
}

class _AppaddState extends State<Appadd> {
  int currentstep = 0 ;
  List<Map<String, dynamic>> _journals = [];

  bool _isLoading = false;
  // This function is used to fetch all data from the database

  void _refreshJournals() async {
    final data = await SQLHelper.getItems();
    setState(() {
      _journals = data;
      _isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    _refreshJournals(); // Loading the diary when the app starts
  }


  void _refreshForm(int url) async {
    await SQLHelper.refreshFormItem(url);
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text('Activer !'),
    ));
    _refreshJournals();


  }



  @override
  Widget build(BuildContext context) =>Scaffold(
    appBar: AppBar(
      backgroundColor: Colors.green,
      title: const Text(
          "Test"
      ),
    ),
    body: Container(
      child: Scaffold(

      body: _isLoading
          ? const Center(
        child: CircularProgressIndicator(),
      )
          : ListView.builder(
        itemCount: _journals.length,
        itemBuilder: (context, index) => Card(
          color: Colors.orange[200],
          margin: const EdgeInsets.all(15),
          child: ListTile(
              title: Text(_journals[index]['nom']),
              subtitle: Text(_journals[index]['url']),
              trailing: SizedBox(
                width: 145,
                child: Row(
                  children: [
                    IconButton(
                      icon: const Icon(Icons.refresh),
                      onPressed: () => _refreshForm(_journals[index]['id']),
                    ),
                  ],
                ),
              )),
        ),
      ),
        bottomNavigationBar: Stepper(
          type: StepperType.horizontal,
          steps: getSteps(),
          currentStep: currentstep,
          onStepContinue: () {
            final isLastStep = currentstep == getSteps().length-1;
            if(isLastStep) {
              setState(()=> currentstep =0);
            }else {
              setState(() => currentstep += 1);
            }
          },
          onStepCancel: () => setState(() => currentstep -=1),
        ),
    ),
    )

  );

  List<Step> getSteps()=>[
    Step(
      isActive: currentstep >=0,
      title: Text("4h"),
      content: Scaffold(

      )


    ),
    Step(
      isActive: currentstep >=1,
      title: const Text("8h"),
      content: Container(),
    ),
    Step(
      isActive: currentstep >=2,
      title: Text("14h"),
      content: Container(),
    ),
    Step(
      isActive: currentstep >=3,
      title: Text("20h"),
      content: Container(),
    ),
    Step(
      isActive: currentstep >=4,
      title: Text("2h"),
      content: Container(),
    )

  ];
}



