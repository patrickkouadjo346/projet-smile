import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter/foundation.dart';
import 'package:sqflite/sqflite.dart' as sql;
import 'package:http/http.dart' as http;
import 'package:path/path.dart';
import 'package:path/path.dart' as p;
import 'package:collection/collection.dart';
import 'package:sqflite/sqflite.dart';



class LocalNotification {
  LocalNotification();


  final _LocalNotification  = FlutterLocalNotificationsPlugin();

  Future<void> initialize() async{
    const AndroidInitializationSettings androidInitializationSettings =
    AndroidInitializationSettings('@drawable/smile');

      IOSInitializationSettings iosInitializationSettings = IOSInitializationSettings(
      requestAlertPermission: true,
      requestBadgePermission: true,
      requestSoundPermission: true,
      onDidReceiveLocalNotification: _onDidReceiveLocal
    );
      final InitializationSettings settings = InitializationSettings(
          android:  androidInitializationSettings,
          iOS: iosInitializationSettings,
      );

      await _LocalNotification.initialize(
        settings,
        onSelectNotification: onSelectNotification,
      );

  }

  void _onDidReceiveLocal( int id, String nom, String url){
    print('id $id');

  }


}

