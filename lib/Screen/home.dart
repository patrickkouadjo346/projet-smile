
import 'dart:convert';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../database/sql_helper.dart';
import 'Search.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  //notification
  void envoinofification(String title , String body) async {
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

    //
    const AndroidInitializationSettings initializationSettingsAndroid =
    AndroidInitializationSettings('@mipmap/smile');
    const IOSInitializationSettings initializationSettingsIOS =
    IOSInitializationSettings(
      requestSoundPermission: true,
      requestBadgePermission: true,
      requestAlertPermission: true,
    );
    final MacOSInitializationSettings initializationSettingsMacOS =
    MacOSInitializationSettings();
    final InitializationSettings initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsIOS,
        macOS: initializationSettingsMacOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
    );

    // android

    AndroidNotificationChannel channel = AndroidNotificationChannel(
        'ok',  'okkk', description: "verifie", importance: Importance.max
    );

    flutterLocalNotificationsPlugin.show(0,title,body,NotificationDetails(
        android: AndroidNotificationDetails(channel.id , channel.name, )
    )
    );


  }





  //
  // All journals
  List<Map<String, dynamic>> _journals = [];

  bool _isLoading = true;
  bool _isActive = true;
  // This function is used to fetch all data from the database
  void _refreshJournals() async {
    final data = await SQLHelper.getItems();
    setState(() {
      _journals = data;
      _isLoading = false;
      _isActive = false ;
    });
  }

  @override
  void initState() {
    super.initState();
    envoinofification;
    _refreshJournals(); // Loading the diary when the app starts
  }

  final TextEditingController _nomController = TextEditingController();
  final TextEditingController _urlController = TextEditingController();



  // This function will be triggered when the floating button is pressed
  // It will also be triggered when you want to update an item
  void _showForm(int id) async {
    if (id != null) {
      // id == null -> create new item
      // id != null -> update an existing item
      final existingJournal =
      _journals.firstWhere((element) => element['id'] == id);
      _nomController.text = existingJournal['nom'];
      _urlController.text = existingJournal['url'];
    }



    showModalBottomSheet(
        context: context,
        elevation: 5,
        isScrollControlled: true,
        builder: (_) => Container(
          padding: EdgeInsets.only(
            top: 15,
            left: 15,
            right: 15,
            // this will prevent the soft keyboard from covering the text fields
            bottom: MediaQuery.of(context).viewInsets.bottom + 120,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              TextField(
                controller: _nomController,
                decoration: const InputDecoration(hintText: 'nom'),
              ),
              const SizedBox(
                height: 10,
              ),
              TextField(
                controller: _urlController,
                decoration: const InputDecoration(hintText: 'url'),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: () async {
                  // Save new journal
                  if (id == null) {
                    await _addItem();
                  }

                  if (id != null) {
                    await _updateItem(id);
                  }

                  // Clear the text fields
                  _nomController.text = '';
                  _urlController.text = '';

                  // Close the bottom sheet
                  Navigator.of(context).pop();
                },
                child: Text(id == null ? 'Ajouter App' : 'Mise a jour'),
              )
            ],
          ),
        ));
  }

// Insert a new journal to the database
  Future<void> _addItem() async {
    await SQLHelper.createItem(
        _nomController.text, _urlController.text);
    _refreshJournals();
  }

  // Update an existing journal
  Future<void> _updateItem(int id) async {
    await SQLHelper.updateItem(
        id, _nomController.text, _urlController.text);
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text('Mise a jour effectué!'),
    ));
    _refreshJournals();
  }

  // Delete an item
  void _deleteItem(int id) async {
    await SQLHelper.deleteItem(id);
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text('App supprimé!'),
    ));
    _refreshJournals();
  }

  void _refreshForm(int url) async {
    await SQLHelper.refreshFormItem(url);
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text('Activer !'),
    ));
    _refreshJournals();


  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Smile App'),
        actions: [
          IconButton(
            onPressed: () {
              //Navigator.push(context, showSearch(context: context) => SearchUser()));
              //showSearch(context: context, delegate: SearchUser());
            },
            icon: Icon(Icons.search_sharp),
          )
        ],
      ),
      body: _isLoading
          ? const Center(
        child: CircularProgressIndicator(),
      )
          : ListView.builder(
        itemCount: _journals.length,
        itemBuilder: (context, index) => Card(
          color: Colors.orange[200],
          margin: const EdgeInsets.all(15),
          child: ListTile(
              title: Text(_journals[index]['nom']),
              subtitle: Text(_journals[index]['url']),
              trailing: SizedBox(
                width: 110,
                child: Row(
                  children: [
                    IconButton(
                      icon: const Icon(Icons.edit),
                      onPressed: () => _showForm(_journals[index]['id']),
                    ),

                    IconButton(
                      icon: const Icon(Icons.delete),
                      onPressed: () =>
                          _deleteItem(_journals[index]['id']),
                    ),
                  ],
                ),
              )),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () => _showForm(null),
      ),
      //bottomNavigationBar: NavBar(),
    );
  }
}