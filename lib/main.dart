import 'package:flutter/material.dart';
import 'package:smiletest/splashscreen/home.dart';
import 'package:smiletest/widget/navbar.dart';

import 'Screen/home.dart';
import 'database/sql_helper.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
FlutterLocalNotificationsPlugin();


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Remove the debug banner
        debugShowCheckedModeBanner: false,
        title: 'Smile App',
        theme: ThemeData(
          primarySwatch: Colors.orange,
        ),
        home: const SplashHome());
  }
}
