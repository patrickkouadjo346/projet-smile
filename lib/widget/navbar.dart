import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../Echec/home.dart';
import '../Screen/home.dart';
import '../Tester/home.dart';
import '../times/Suivi.dart';

class NavBar extends StatefulWidget {

  final VoidCallback login;
  NavBar({this.login});

  @override
  State<NavBar> createState() => _NavBarState();
}


class _NavBarState extends State<NavBar> {

  void envoinofification(String title , String body) async {
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

    //
    const AndroidInitializationSettings initializationSettingsAndroid =
    AndroidInitializationSettings('@mipmap/smile');
    const IOSInitializationSettings initializationSettingsIOS =
    IOSInitializationSettings(
      requestSoundPermission: true,
      requestBadgePermission: true,
      requestAlertPermission: true,
    );
    final MacOSInitializationSettings initializationSettingsMacOS =
    MacOSInitializationSettings();
    final InitializationSettings initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsIOS,
        macOS: initializationSettingsMacOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
    );

    // android

    AndroidNotificationChannel channel = AndroidNotificationChannel(
        'ok',  'okkk', description: "verifie", importance: Importance.max
    );

    flutterLocalNotificationsPlugin.show(0,title,body,NotificationDetails(
      android: AndroidNotificationDetails(channel.id , channel.name, )
    )
    );


  }

  int index = 0;
  final navigationkey = GlobalKey<CurvedNavigationBarState>();
  final screens =
  [
    HomePage(),
    //Appadd(),
    Suivi(),
    Echec(),
  ];

  @override
  Widget build(BuildContext context) {
    final items =[
      Icon(Icons.home, size: 19),
      Icon(Icons.sync, size: 15),
      Icon(Icons.error, size: 19),
    ];

    return Scaffold(
      body: screens [index],

      bottomNavigationBar: CurvedNavigationBar(
        height: 60,
        items: items,
        index: index,
        onTap: (index) => setState(()=> this.index = index),
      ),
    );
  }
}