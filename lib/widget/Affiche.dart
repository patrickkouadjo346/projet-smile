import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import '../database/sql_helper.dart';

class Affiche extends StatefulWidget {
  const Affiche({Key key}) : super(key: key);

  @override
  State<Affiche> createState() => _AfficheState();
}

class _AfficheState extends State<Affiche> {

  // notification

  void envoinofification(String title , String body) async {
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

    //
    const AndroidInitializationSettings initializationSettingsAndroid =
    AndroidInitializationSettings('@mipmap/smile');
    const IOSInitializationSettings initializationSettingsIOS =
    IOSInitializationSettings(
      requestSoundPermission: true,
      requestBadgePermission: true,
      requestAlertPermission: true,
    );
    final MacOSInitializationSettings initializationSettingsMacOS =
    MacOSInitializationSettings();
    final InitializationSettings initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsIOS,
        macOS: initializationSettingsMacOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
    );


    // android

    AndroidNotificationChannel channel = AndroidNotificationChannel(
        'ok',  'okkk', description: "verifie", importance: Importance.max
    );

    flutterLocalNotificationsPlugin.show(0,title,body,NotificationDetails(
        android: AndroidNotificationDetails(channel.id , channel.name, )
    )
    );


  }


  List<Map<String, dynamic>> _journals = [];

  bool _isLoading = true;
  bool _isActive = true;
  // This function is used to fetch all data from the database
  void _refreshJournals() async {
    final data = await SQLHelper.getItems();
    setState(() {
      _journals = data;
      _isLoading = false;
      _isActive = false ;
    });
  }

  @override
  void initState() {
    super.initState();
    refreshFormItem;
    envoinofification;
    _refreshJournals(); // Loading the diary when the app starts
  }

  final TextEditingController _nomController = TextEditingController();
  final TextEditingController _urlController = TextEditingController();



  // This function will be triggered when the floating button is pressed
  // It will also be triggered when you want to update an item

// Insert a new journal to the database


  void _refreshForm(int url) async {
    await SQLHelper.refreshFormItem(url);
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text('Activer !'),
    ));
    _refreshJournals();


  }

  void fonction (int url) async{

  }

  String reponse ;
  String er = 'Api Fonctionne pas  !';
  String ok = 'Api Fonctionne  !';

   Future<void> refreshFormItem(String url) async{
    final response = await http.post(Uri.parse(url));
    try{
      if (response.statusCode == 200){
        envoinofification('' , "verification effectuer");
      }else{
        envoinofification('' , "verification echec");
      }
    } catch (err) {
      debugPrint("Erreur : $err");
    }

  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _isLoading
          ? const Center(
        child: CircularProgressIndicator(),
      )
          : ListView.builder(
        itemCount: _journals.length,
        itemBuilder: (context, index) => Card(
          color: Colors.green[200],
          margin: const EdgeInsets.all(15),
          child: ListTile(
              title: Text(_journals[index]['nom']),
              subtitle: Text(_journals[index]['url']),
              trailing: SizedBox(
                width: 110,
                child: Row(
                  children: [

                    IconButton(
                      icon:  Icon(Icons.refresh),
                      onPressed: () => refreshFormItem(_journals[index]['url']),
                    ),


                  ],
                ),
              )),
        ),
      ),

    );
  }
}
