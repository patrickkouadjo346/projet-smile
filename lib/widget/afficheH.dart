import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../database/sql_helper.dart';

class AfficheH extends StatefulWidget {
  const AfficheH({Key key}) : super(key: key);

  @override
  State<AfficheH> createState() => _AfficheHState();
}

class _AfficheHState extends State<AfficheH> {
  List<Map<String, dynamic>> _journals = [];

  bool _isLoading = true;
  bool _isActive = true;
  // This function is used to fetch all data from the database
  void _refreshJournals() async {
    final data = await SQLHelper.getItems();
    setState(() {
      _journals = data;
      _isLoading = false;
      _isActive = false ;
    });
  }

  @override
  void initState() {
    super.initState();
    _refreshJournals(); // Loading the diary when the app starts
  }

  final TextEditingController _nomController = TextEditingController();
  final TextEditingController _urlController = TextEditingController();



  // This function will be triggered when the floating button is pressed
  // It will also be triggered when you want to update an item

// Insert a new journal to the database


  void _refreshForm(int url) async {
    await SQLHelper.refreshFormItem(url);
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text('Activer !'),
    ));
    _refreshJournals();


  }

  Future<void> refreshFormItem(String url) async{
    final response = await http.post(Uri.parse(url));
    try{
      if (response.statusCode == 200){
        var ok =ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('Api Fonctionne !'),
        ));
        return ok;
      }else{
        var err =ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('Api Fonctionne pas  !'),
        ));
        return err;
      }
    } catch (err) {
      debugPrint("Erreur : $err");
    }

  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _isLoading
          ? const Center(
        child: CircularProgressIndicator(),
      )
          : ListView.builder(
        itemCount: _journals.length,
        itemBuilder: (context, index) => Card(
          color: Colors.green[200],
          margin: const EdgeInsets.all(15),
          child: ListTile(
              title: Text(_journals[index]['nom']),
              subtitle: Text(_journals[index]['url']),
              trailing: SizedBox(
                width: 70,
                child: Row(
                  children: [
                    IconButton(
                      icon: const Icon(Icons.refresh),
                      onPressed: () => refreshFormItem(_journals[index]['url']),
                    ),

                  ],
                ),
              )),
        ),
      ),

    );
  }
}


