import 'package:flutter/cupertino.dart';
import 'package:animated_splash/animated_splash.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:lottie/lottie.dart';
import '../Menu/menu.dart';
import '../widget/navbar.dart';

class SplashHome extends StatelessWidget {
  const SplashHome({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      //splash: Lottie.network("https://assets8.lottiefiles.com/packages/lf20_ko8ud57v.json"),
      splash: Lottie.asset("assets/spla/center.json"),

      splashIconSize:270 ,
      duration: 7000,
      //nextScreen: NavBar(),
      nextScreen: MakeDashboardItems(),
      //nextScreen:  HomePage(),

    );
  }
}
