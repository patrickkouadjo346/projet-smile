import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:lottie/lottie.dart';

import '../projet /list.dart';
import '../projet /projet.dart';

class SProjet extends StatefulWidget {
  const SProjet({Key key}) : super(key: key);

  @override
  State<SProjet> createState() => _SProjetState();
}

class _SProjetState extends State<SProjet> {
  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      //splash: Lottie.network("https://assets8.lottiefiles.com/packages/lf20_ko8ud57v.json"),
      splash: Lottie.asset("assets/spla/projet.json"),

      splashIconSize:270 ,
      duration: 4000,
      //nextScreen: NavBar(),
      nextScreen: IntrProjet(),
      //nextScreen:  HomePage(),

    );
  }
}

