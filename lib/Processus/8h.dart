import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:http/http.dart' as http;
import '../database/sql_helper.dart';
import '../widget/Affiche.dart';

class H8h extends StatefulWidget {
  const H8h({Key key}) : super(key: key);

  @override
  State<H8h> createState() => _H8hState();
}

class _H8hState extends State<H8h> {
  DateTime Time;

  List<Map<String, dynamic>> _journals = [];

  bool _isLoading = true;
  bool _isActive = true;
  // This function is used to fetch all data from the database
  void _refreshJournals() async {
    final data = await SQLHelper.getItems();
    setState(() {
      _journals = data;
      _isLoading = false;
      _isActive = false ;
    });
  }

  @override
  void initState() {
    super.initState();
    getCount();
    _refreshJournals(); // Loading the diary when the app starts
  }
  int number = 0;
  void getCount() async{
    int count = await SQLHelper.getCount();
    setState (() {
      number = count;
    });
  }

  final TextEditingController _nomController = TextEditingController();
  final TextEditingController _urlController = TextEditingController();



  // This function will be triggered when the floating button is pressed
  // It will also be triggered when you want to update an item

// Insert a new journal to the database

  int succes = 0;
  int echec = 0;

  Future<void> refreshFormItem(String url) async{
    final response = await http.post(Uri.parse(url));
    try{
      if (response.statusCode == 200){
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('Api Fonctionne !'),
        ));
        return succes;
      }else{
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('Api Fonctionne pas  !'),
        ));
        return echec;
      }
    } catch (err) {
      debugPrint("Erreur : $err");
    }
    _refreshJournals();

  }



  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
      appBar: AppBar(
        title: Text('Static 8h'),
      ),
      body: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(child: Container(
              padding: const EdgeInsets.all(15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularPercentIndicator(
                    footer: Text('succes'),
                    radius: 45.0,
                    lineWidth: 4.0,
                    animation: true,
                    percent: 1,
                    center: Text("$succes/$number"),
                    progressColor: Colors.green,
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10.0),
                  ),
                  CircularPercentIndicator(
                    radius: 45.0,
                    lineWidth: 4.0,
                    footer: Text('Total'),
                    animation: true,
                    center: Text("$number/$number"),
                    progressColor: Colors.black,
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10.0),
                  ),
                  CircularPercentIndicator(
                    radius: 45.0,
                    footer: Text('echec'),
                    lineWidth: 4.0,
                    percent: 1,
                    animation: true,
                    center: Text("$echec/$number"),
                    progressColor: Colors.red,
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10.0),
                  ),
                ],
              ),
            ),),
            const SizedBox(height: 10.0,),
            const Expanded(child: Affiche())
          ],
        ),
      )
    ),

    );
  }
}
