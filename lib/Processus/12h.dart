import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

import '../database/sql_helper.dart';
import '../widget/Affiche.dart';

class H2h extends StatefulWidget {
  const H2h({Key key}) : super(key: key);

  @override
  State<H2h> createState() => _H2hState();
}

class _H2hState extends State<H2h> {
  List<Map<String, dynamic>> _journals = [];

  bool _isLoading = true;
  bool _isActive = true;
  // This function is used to fetch all data from the database
  void _refreshJournals() async {
    final data = await SQLHelper.getItems();
    setState(() {
      _journals = data;
      _isLoading = false;
      _isActive = false ;
    });
  }

  @override
  void initState() {
    super.initState();
    _refreshJournals(); // Loading the diary when the app starts
  }

  final TextEditingController _nomController = TextEditingController();
  final TextEditingController _urlController = TextEditingController();



  // This function will be triggered when the floating button is pressed
  // It will also be triggered when you want to update an item

// Insert a new journal to the database


  void _refreshForm(int url) async {
    await SQLHelper.refreshFormItem(url);
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text('Activer !'),
    ));
    _refreshJournals();


  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
        appBar: AppBar(
          title: Text('Static 12h'),
        ),
        body: Container(
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(child: Container(
                padding: const EdgeInsets.all(15.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircularPercentIndicator(
                      radius: 45.0,
                      lineWidth: 4.0,
                      animation: true,
                      percent: 0.10,
                      center: const Text("10%"),
                      progressColor: Colors.green,
                    ),
                    const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10.0),
                    ),
                    CircularPercentIndicator(
                      radius: 45.0,
                      lineWidth: 4.0,
                      animation: true,
                      percent: 0.30,
                      center: const Text("30%"),
                      progressColor: Colors.black,
                    ),
                    const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10.0),
                    ),
                    CircularPercentIndicator(
                      radius: 45.0,
                      lineWidth: 4.0,
                      percent: 0.60,
                      animation: true,
                      center: const Text("60%"),
                      progressColor: Colors.red,
                    ),
                    const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10.0),
                    ),
                  ],
                ),
              ),),
              const SizedBox(height: 10.0,),
              const Expanded(child: Affiche())
            ],
          ),
        )
    ),

    );
  }
}