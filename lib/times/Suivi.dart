import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:smiletest/Processus/12h.dart';
import 'package:sqflite/sqflite.dart';
import 'package:smiletest/database/sql_helper.dart';

import '../Processus/16h.dart';
import '../Processus/22h.dart';
import '../Processus/8h.dart';

class Suivi extends StatefulWidget {
  const Suivi({Key key}) : super(key: key);

  @override
  State<Suivi> createState() => _SuiviState();
}

class _SuiviState extends State<Suivi> {
  //notification
  void envoinofification(String title, String body) async {
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();

    //
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/smile');
    const IOSInitializationSettings initializationSettingsIOS =
        IOSInitializationSettings(
      requestSoundPermission: true,
      requestBadgePermission: true,
      requestAlertPermission: true,
    );
    final MacOSInitializationSettings initializationSettingsMacOS =
        MacOSInitializationSettings();
    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: initializationSettingsIOS,
            macOS: initializationSettingsMacOS);
    await flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
    );

    // android

    AndroidNotificationChannel channel = AndroidNotificationChannel(
        'ok', 'okkk',
        description: "verifie", importance: Importance.max);

    flutterLocalNotificationsPlugin.show(
        0,
        title,
        body,
        NotificationDetails(
            android: AndroidNotificationDetails(
          channel.id,
          channel.name,
        )));
  }

  //

  List<Map<String, dynamic>> _journals = [];
  bool _isLoading = true;
  bool _isActive = true;
  int index;
  String H;
  String H8 = '8H00';
  String H12 = '12H00';
  String H16 = '16H00';
  String H22 = '22H00';

  // This function is used to fetch all data from the database
  void _refreshJournals() async {
    final data = await SQLHelper.getItems();
    setState(() {
      _journals = data;
      _isLoading = false;
      _isActive = false;

      _condition(_journals);
    });
  }

  @override
  void initState() {
    getCount();
    super.initState();
    _refreshJournals(); // Loading the diary when the app starts
  }

  String Time;
  int number = 0;

  void getCount() async {
    int count = await SQLHelper.getCount();
    setState(() {
      number = count;
    });
  }

  //void getCount() async {
  //await SQLHelper.getCount();
  //setState (() {
  //number = count;
  //});

  //}

  //app timer periodic

  Future<void> refreshFormItem(String url) async {
    final response = await http.post(Uri.parse(url));
    try {
      if (response.statusCode == 200) {
        //envoinofification( $H , "verification effectuée");
      } else {
        //envoinofification( H, "verification echec");
      }
    } catch (err) {
      debugPrint("Erreur : $err");
    }
  }

  //erreur

  Future<void> ErefreshFormItem(String url) async {
    final response = await http.post(Uri.parse(url));
    try {
      if (response.statusCode == 200) {
      } else {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('Api Fonctionne pas  !'),
        ));
      }
    } catch (err) {
      debugPrint("Erreur : $err");
    }
  }

  void _condition(List<Map<String, dynamic>> pJournals) {
    var now = DateTime.now();
    var fomatime = DateFormat('HH:mm').format(now);

    for (var element in pJournals) {
      print("Url => ${element['url']}");

      Future<void> refreshFormItem(String url) async {
        final response = await http.post(Uri.parse(url));
        try {
          if (response.statusCode == 200) {
            envoinofification('${element['nom']}''  ' '$H', "verification effectuer");

          } else {
            envoinofification('${element['nom']}' '  ' '$H', "verification echec");
          }
        } catch (err) {
          debugPrint("Erreur : $err");
        }
      }

      if (fomatime == '8HOO') {
        H = H8;
        refreshFormItem(element['url']);
      } else if (fomatime == '14H00') {
        H = H12;
        refreshFormItem(element['url']);
      } else if (fomatime == '16:00') {
        H = H16;
        refreshFormItem(element['url']);
      } else if (fomatime == '22:00') {
        H = H22;
        refreshFormItem(element['url']);
      } else {
        envoinofification('${element['nom']}' , "verification en attente ");
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Stat Api "),
      ),
      body: Center(
        child: ListView(children: <Widget>[
          CircularPercentIndicator(
            radius: 120.0,
            lineWidth: 13.0,
            animation: true,
            percent: 1,
            center: Text(
              "$number",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
            ),
            footer: Text(
              "Api en direct ",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17.0),
            ),
            circularStrokeCap: CircularStrokeCap.round,
            progressColor: Colors.purple,
          ),
          const SizedBox(
            height: 40,
          ),
          Center(
            child: TextButton(
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => H8h()));
              },
              child: const Text(
                "8h",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                    color: Colors.black),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircularPercentIndicator(
                  radius: 45.0,
                  lineWidth: 4.0,
                  animation: true,
                  percent: 0.10,
                  center: const Text("Api ok 10%"),
                  progressColor: Colors.green,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                ),
                CircularPercentIndicator(
                  radius: 45.0,
                  lineWidth: 4.0,
                  animation: true,
                  percent: 0.30,
                  center: const Text("30%"),
                  progressColor: Colors.black,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                ),
                CircularPercentIndicator(
                  radius: 45.0,
                  lineWidth: 4.0,
                  percent: 0.60,
                  animation: true,
                  center: const Text("60%"),
                  progressColor: Colors.red,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                ),
              ],
            ),
          ),
          Center(
            child: TextButton(
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => H2h()));
              },
              child: Text(
                "12h",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                    color: Colors.black),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircularPercentIndicator(
                  radius: 45.0,
                  lineWidth: 4.0,
                  animation: true,
                  percent: 0.10,
                  center: const Text("10%"),
                  progressColor: Colors.green,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                ),
                CircularPercentIndicator(
                  radius: 45.0,
                  lineWidth: 4.0,
                  animation: true,
                  percent: 0.30,
                  center: const Text("30%"),
                  progressColor: Colors.black,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                ),
                CircularPercentIndicator(
                  radius: 45.0,
                  lineWidth: 4.0,
                  percent: 0.60,
                  animation: true,
                  center: const Text("60%"),
                  progressColor: Colors.red,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                ),
              ],
            ),
          ),
          Center(
            child: TextButton(
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => H4h()));
              },
              child: Text(
                "16h",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                    color: Colors.black),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircularPercentIndicator(
                  radius: 45.0,
                  lineWidth: 4.0,
                  animation: true,
                  percent: 0.10,
                  center: const Text("10%"),
                  progressColor: Colors.green,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                ),
                CircularPercentIndicator(
                  radius: 45.0,
                  lineWidth: 4.0,
                  animation: true,
                  percent: 0.30,
                  center: const Text("30%"),
                  progressColor: Colors.black,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                ),
                CircularPercentIndicator(
                  radius: 45.0,
                  lineWidth: 4.0,
                  percent: 0.60,
                  animation: true,
                  center: const Text("60%"),
                  progressColor: Colors.red,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                ),
              ],
            ),
          ),
          Center(
            child: TextButton(
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => H20h()));
              },
              child: Text(
                "22h",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                    color: Colors.black),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircularPercentIndicator(
                  radius: 45.0,
                  lineWidth: 4.0,
                  animation: true,
                  percent: 0.10,
                  center: const Text("10%"),
                  progressColor: Colors.green,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                ),
                CircularPercentIndicator(
                  radius: 45.0,
                  lineWidth: 4.0,
                  animation: true,
                  percent: 0.30,
                  center: const Text("30%"),
                  progressColor: Colors.black,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                ),
                CircularPercentIndicator(
                  radius: 45.0,
                  lineWidth: 4.0,
                  percent: 0.60,
                  animation: true,
                  center: const Text("60%"),
                  progressColor: Colors.red,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                ),
              ],
            ),
          ),
        ]),
      ),
    );
  }
}
