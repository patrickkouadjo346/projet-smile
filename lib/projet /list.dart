import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:smiletest/projet%20/sql%20Projet.dart';
import 'package:intl/intl.dart';
import 'add projet.dart';
import 'dart:async';
import 'descriptionProjet.dart';

class Projet extends StatefulWidget {
  const Projet({Key key}) : super(key: key);

  @override
  State<Projet> createState() => _ProjetState();
}

class _ProjetState extends State<Projet> {

  void envoinofification(String title , String body) async {
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

    //
    const AndroidInitializationSettings initializationSettingsAndroid =
    AndroidInitializationSettings('@mipmap/smile');
    const IOSInitializationSettings initializationSettingsIOS =
    IOSInitializationSettings(
      requestSoundPermission: true,
      requestBadgePermission: true,
      requestAlertPermission: true,
    );
    final MacOSInitializationSettings initializationSettingsMacOS =
    MacOSInitializationSettings();
    final InitializationSettings initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsIOS,
        macOS: initializationSettingsMacOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
    );

    // android

    AndroidNotificationChannel channel = AndroidNotificationChannel(
        'ok',  'okkk', description: "verifie", importance: Importance.max
    );

    flutterLocalNotificationsPlugin.show(0,title,body,NotificationDetails(
        android: AndroidNotificationDetails(channel.id , channel.name, )
    )
    );


  }

  String taskName;
  DateTime taskTime;
  DateTime taskDate=DateTime.now();
  datePicker() async{
    final DateTime date= await showDatePicker(
      context: context,
      initialDate: taskDate,
      firstDate: DateTime(2020),
      lastDate: DateTime(2050),
    );
    setState(() {
      taskDate=date;
    });
    _dateDController.text= dateFormat.format(date);

  }

  datePicker1() async{
    final DateTime date= await showDatePicker(
      context: context,
      initialDate: taskDate,
      firstDate: DateTime(2020),
      lastDate: DateTime(2050),
    );
    setState(() {
      taskDate=date;
    });
    _dateFController.text= dateFormat.format(date);

  }
  DateFormat dateFormat = DateFormat('dd MMM,yyyy');



  //
  // All journals
  List<Map<String, dynamic>> _journals = [];

  bool _isLoading = true;
  bool _isActive = true;
  // This function is used to fetch all data from the database
  void _refreshJournals() async {
    final data = await SQLHelperP.getProjet();
    setState(() {
      _journals = data;
      _isLoading = false;
      _isActive = false ;
    });
  }

  @override
  void initState() {
    super.initState();
    envoinofification;
    _refreshJournals(); // Loading the diary when the app starts
  }

  final TextEditingController _nomController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final TextEditingController _dateDController = TextEditingController();
  final TextEditingController _dateFController = TextEditingController();



  // This function will be triggered when the floating button is pressed
  // It will also be triggered when you want to update an item
  void _showForm(int id) async {
    if (id != null) {
      // id == null -> create new item
      // id != null -> update an existing item
      final existingJournal =
      _journals.firstWhere((element) => element['id'] == id);
      _nomController.text = existingJournal['nom'];
      _descriptionController.text = existingJournal['description'];
      _dateDController.text = existingJournal['dateD'];
      _dateFController.text = existingJournal['dateF'];
    }


    showModalBottomSheet(
        context: context,
        elevation: 5,
        isScrollControlled: true,
        builder: (_) => Container(
          padding: EdgeInsets.only(
            top: 15,
            left: 15,
            right: 15,
            // this will prevent the soft keyboard from covering the text fields
            bottom: MediaQuery.of(context).viewInsets.bottom + 120,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              TextField(
                controller: _nomController,
                decoration: InputDecoration(
                  labelText: 'Nom Projet',
                  labelStyle: TextStyle(fontSize: 18 ),
                  border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.redAccent),
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              TextField(
                controller: _descriptionController,
                decoration: InputDecoration(
                  labelText: 'Description',
                  labelStyle: TextStyle(fontSize: 18 ),
                  border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.redAccent),
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),

              TextField(
                onTap: datePicker,
                controller: _dateDController,
                decoration: InputDecoration(
                  labelText: 'Date Debut',
                  labelStyle: TextStyle(fontSize: 18 ),
                  border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.redAccent),
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                ),
              ),

              const SizedBox(
                height: 20,
              ),

              TextField(
                onTap: datePicker1,
                controller: _dateFController,
                decoration: InputDecoration(
                  labelText: 'Date Fin',
                  labelStyle: TextStyle(fontSize: 18 ),
                  border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.redAccent),
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: () async {
                  // Save new journal
                  if (id == null) {
                    await _addProjet();
                  }

                  if (id != null) {
                    await _updateProjet(id);
                  }

                  // Clear the text fields
                  _nomController.text = '';
                  _descriptionController.text = '';
                  _dateDController.text = '';
                  _dateFController.text = '';

                  // Close the bottom sheet
                  Navigator.of(context).pop();
                },
                child: Text(id == null ? 'Ajouter App' : 'Mise a jour'),
              )
            ],
          ),
        ));
  }

// Insert a new journal to the database
  Future<void> _addProjet() async {
    await SQLHelperP.createProjet(
        _nomController.text, _descriptionController.text, _dateDController.text, _dateFController.text);
    _refreshJournals();
  }

  // Update an existing journal
  Future<void> _updateProjet(int id) async {
    await SQLHelperP.updateProjet(
        id, _nomController.text, _descriptionController.text,_dateDController.text, _dateFController.text);
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text('Mise a jour effectué!'),
    ));
    _refreshJournals();
  }

  // Delete an item
  void _deleteProjet(int id) async {
    await SQLHelperP.deleteProjet(id);
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text('App supprimé!'),
    ));
    _refreshJournals();
  }


  void _refreshForm(int url) async {
    await SQLHelperP.refreshFormProjet(url);
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text('Activer !'),
    ));
    _refreshJournals();


  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: const Text('Projet',
        style: TextStyle(backgroundColor: Colors.transparent),
        ),
        actions: [
          IconButton(
              onPressed: () => _showForm(null),

            icon: Icon(Icons.add),
          )
        ],
      ),
      body: _isLoading
          ? const Center(
        child: CircularProgressIndicator(),
      )
          : ListView.builder(
        itemCount: _journals.length,
        itemBuilder: (context, index) => Card(
          color: Colors.orange[200],
          margin: const EdgeInsets.all(15),
          child: ListTile(
              title: Text(_journals[index]['nom']),
              subtitle: Text(_journals[index]['description']),
              leading: Text(_journals[index]['dateD']),
              onTap: (){
                Navigator.of(context).push(MaterialPageRoute(builder: (context) =>Dprojet(projet: null,),
                ));
              },
              trailing: SizedBox(
                width: 100,
                child: Row(
                  children: [
                    Text(_journals[index]['dateF'])
                    /*IconButton(
                      icon:  Icon(Icons.keyboard_arrow_right),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Dprojet()),
                        );
                      },
                    ),*/

                    /*IconButton(
                      icon: const Icon(Icons.edit),
                      onPressed: () => _showForm(_journals[index]['id']),
                    ),

                    IconButton(
                      icon: const Icon(Icons.delete),
                      onPressed: () =>
                          _deleteProjet(_journals[index]['id']),
                    ),*/
                  ],
                ),
              )),
        ),
      ),
      //bottomNavigationBar: NavBar(),
    );
  }
}