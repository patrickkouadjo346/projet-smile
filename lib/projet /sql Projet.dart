import 'package:flutter/cupertino.dart';
import 'package:sqflite/sqflite.dart' as sql;
import 'package:path/path.dart' as path;
import 'package:sqflite/sqflite.dart';
import 'package:http/http.dart' as http;
import 'package:sqflite/sqlite_api.dart';
/*
class DBHelper {
  static Future<Database> database() async {
    final dbPath = await sql.getDatabasesPath();
    return await sql.openDatabase(path.join(dbPath, 'angesmile.db'), onCreate: (db, version) => createDb(db), version: 1);
  }

  static void createDb(Database db) {
    db.execute("CREATE TABLE notes (id TEXT PRIMARY KEY, title TEXT, date TEXT)");
    db.execute("CREATE TABLE descs (id TEXT PRIMARY KEY, title TEXT)");
    db.execute("CREATE TABLE note_descs (id TEXT PRIMARY KEY, note_id TEXT, desc_id TEXT)");
  }

  static Future<void> insert(String table, Map<String, Object> data) async {
    final db = await DBHelper.database();
    db.insert(table, data, conflictAlgorithm: sql.ConflictAlgorithm.replace);
  }

  static Future<void> delete(String table, String id) async {
    final db = await DBHelper.database();
    return db.delete(table, where: 'id = ?', whereArgs: [id]);
  }

  static Future<List<Map<String, dynamic>>> getData() async {
    final db = await DBHelper.database();
    return db.rawQuery("SELECT a.date, a.id note_id, b.id note_desc_id, c.id desc_id, GROUP_CONCAT(DISTINCT a.title) parentTitle, c.title childTitle FROM notes a LEFT JOIN note_descs b ON a.id = b.note_id LEFT JOIN descs c ON c.id = b.desc_id GROUP BY a.id");
  }
}*/



class SQLHelperP {
  static Future<void> createTables(sql.Database database) async {
    await database.execute("""CREATE TABLE projet(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        nom TEXT,
        description TEXT,
        dateD TEXT,
        dateF TEXT,
        createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
      )
      """);
  }




  static Future<sql.Database> db() async {
    return sql.openDatabase(
      'angesmil.db',
      version: 1,
      onCreate: (sql.Database database, int version) async {
        await createTables(database);
      },
    );
  }

// id: the id of a item
// title, description: name and description of your activity
// created_at: the time that the item was created. It will be automatically handled by SQLite


  // creer  nouveau  projet



  static Future<int> createProjet(String nom, String description,String dateD, String dateF) async {
    final db = await SQLHelperP.db();

    final data = {'nom': nom, 'description': description, 'dateD': dateD, 'dateF': dateF};
    final id = await db.insert('projet', data,
        conflictAlgorithm: sql.ConflictAlgorithm.replace);
    return id;
  }

  // lire tous les  items
  static Future<List<Map<String, dynamic>>> getProjet() async {
    final db = await SQLHelperP.db();
    return db.query('projet', orderBy: "id");
  }

  // Read a single item by id
  // The app doesn't use this method but I put here in case you want to see it
  static Future<List<Map<String, dynamic>>> getprojet(int id) async {
    final db = await SQLHelperP.db();
    return db.query('projet', where: "id = ?", whereArgs: [id], limit: 1);
  }

  // modifier  item  id
  static Future<int> updateProjet(
      int id, String nom,String description,String dateD, String dateF) async {
    final db = await SQLHelperP.db();

    final data = {
      'nom': nom,
      'description': description,
      'dateD': dateD,
      'dateF': dateF,
      'createdAt': DateTime.now().toString()
    };

    final result =
    await db.update('projet', data, where: "id = ?", whereArgs: [id]);
    return result;
  }

  // Delete
  static Future<void> deleteProjet(int id) async {
    final db = await SQLHelperP.db();
    try {
      await db.delete("projet", where: "id = ?", whereArgs: [id]);
    } catch (err) {
      debugPrint("Something went wrong when deleting an projet: $err");
    }
  }

  //count
  static Future<int> getConProjet() async{
    var db = await SQLHelperP.db();
    List<Map<String, dynamic>> x = await db.rawQuery("SELECT COUNT (*) from projet");
    int count = Sqflite.firstIntValue(x);
    print(
        '$count'
    );
    return count;
  }

  static Future<int> getConProjets() async{
    var db = await SQLHelperP.db();
    final count = Sqflite.firstIntValue(await db.rawQuery("SELECT COUNT(*) from projet"));
    print('$count');
    return count;
  }



  //api url verification (non actif)



  static Future<void> refreshFormProjet(int url) async{
    final response = await http.post(Uri.parse("https://test132smile.000webhostapp.com/ajoutApp.php"

    ),
    );
    try{
      if (response.statusCode == 200){
        print("ok");
      }else{
        print("erreur");
      }
    } catch (err) {
      debugPrint("Erreur : $err");
    }

  }

  //

// fontions de recherche
  static Future<List> searchProjet(String app) async{
    var db = await SQLHelperP.db();
    List<Map<String, dynamic>> search = await db.query('projet', where: 'nom LIKE ?', whereArgs: ['%$app%']);
    return search;
  }


//fin

//fonction pour actualiser aux heures







}